/*
 ============================================================================
 Name        : Izziv1.c
 Author      : Miha Jamsek
 Version     : v1.0.0
 Copyright   : 
 Description : Program, ki prebere datoteko ne glede na to kateri uporabnik je pognal program
 ============================================================================
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define CMD "chmod a+r "
#define FILENAME "/etc/shadow"
#define MAX_LINE_LEN 1000
#define MAX_CMD_LEN 150

unsigned int vrniFilePermission(){
	struct stat fileStat;
	mode_t file_permission;
	if(stat(FILENAME, &fileStat) == -1){
		perror("Napaka pri branju pravic datoteke!");
		exit(EXIT_FAILURE);
	}

	file_permission = fileStat.st_mode;
	return (unsigned int) file_permission%01000;
}

int main(void) {

	//shrani dovoljenja datoteke
	unsigned int dovoljenjaDatoteke = vrniFilePermission();
	//init ukaza za dodajanje pravic
	char ukaz[50], dat[50];
	strcpy(ukaz,  CMD);
	strcpy(dat, FILENAME);
	strcat(ukaz, dat);
	//dodaj pravice
	system(ukaz);

	//odpri datoteko in izpisi njeno vsebino
	FILE *f = fopen(FILENAME, "r");
	if(f != NULL){
		char* t = (char *) malloc(MAX_LINE_LEN * sizeof(char));

		while(fscanf(f, "%s", t) != EOF){
			printf("%s ", t);
		}
	} else {
		printf("Napaka pri odpiranju datoteke!\n");
		exit(EXIT_FAILURE);
	}

	//povrni uporabniska dovoljenja
	char restore[50];
	int perms = sprintf(restore, "chmod %o %s", dovoljenjaDatoteke, FILENAME);
	system(restore);

	return EXIT_SUCCESS;
}
